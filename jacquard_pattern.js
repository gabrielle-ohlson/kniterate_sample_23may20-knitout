// as functions with relaceable carrier variable

// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
const center_needle = Math.round(((left_needle - (right_needle - 1))/2) + right_needle);
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on back bed in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}
////////////////////
// jacquard funcs //
////////////////////
// func for knitting all needles on front bed & tucking on all N on back bed in pos dir, once
function knitFTuckBPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.tuck('+', 'b' + s, carrier);
    }
}
// func for knitting end needles on one bed and all other needles on other bed
function knitEndNElseMid() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (s === left_needle || s === right_needle) {
                k.knit('+', end_needle + s, carrier);
            } else {
                k.knit('+', mid_needle + s, carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s === right_needle || s === left_needle) {
                k.knit('-', end_needle + s, carrier);
            } else {
                k.knit('-', mid_needle + s, carrier);
            }
        }
    }
}
// funcs for pushing selected needles to jacquard arrays with variable number of misses in between
function arrJacPush_1(number) {
    i += number;
    array.push(i);
    ++i;
    array.push(i);
}
function arrJacPush_2(number) {
    i += number;
    for (let p = 0; p < 5; ++p) {
        array.push(i);
        if (p < 4) {
            ++i;
        }
    }
}
// generate base array for 1st knit sequence in jacquard pat, altered according to left/right-most needle variables so can be reused (IIFE)
let arr_jac_1 = [];
(function arrJacRange() {
    i = left_needle;
    array = arr_jac_1;
    array.push(i);
    arrJacPush_1(8);
    arrJacPush_1(3);
    arrJacPush_1(4);
    arrJacPush_1(5);
    arrJacPush_1(5);
    arrJacPush_1(7);
    arrJacPush_2(6);
    arrJacPush_1(5);
    arrJacPush_1(3);
    arrJacPush_1(5);
    arrJacPush_1(5);
    arrJacPush_1(6);
    arrJacPush_2(6);
    i += 8;
    array.push(i);
})();
// func for first jacquard knit
function jacKnit_1() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_jac_1.includes(s) === true) {
                k.knit('+', bed_1 + s, carrier);
            }
            if (arr_jac_1.includes(s) !== true) {
                k.knit('+', bed_2 + s, carrier);
            }
        }
    } else {    
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_jac_1.includes(s) === true) {
                k.knit('-', bed_1 + s, carrier);
            }
            if (arr_jac_1.includes(s) !== true) {
                k.knit('-', bed_2 + s, carrier);
            }
        }
    }
}
// insert with splice, which alters original array
function insert(array, index, ...items) {
    array.splice(index, 0, ...items);
}
// // insert with slice, which makes new array instead
// function insert(array, index, ...items) {
//     return [
// 		...array.slice(0, index),
// 		...items,
// 		...array.slice(index)
// 	];
// }
// let result = insert(arr_jac_1, 2, (arr_jac_1[2] + 1));
function remove(array, index, delete_count, ...items) {
    array.splice(index, delete_count, ...items);
}
// generate base arrays for 2nd knit sequence in jacquard pat (kind of like a part 2), altered according to left/right-most needle variables so can be reused (IIFE)
let arr_jac2_1 = [];
let arr_jac2_2 = [];
(function arrJac2Range() {
    arr_jac2_1.push(left_needle);
    arr_jac2_1.push(right_needle);
    arr_jac2_2.push(left_needle);
    arr_jac2_2.push(right_needle);
    m = center_needle;
    arr_jac2_2.push(m);
    m -= 11;
    arr_jac2_1.push(m);
    --m;
    arr_jac2_1.push(m);
    m -= 11;
    arr_jac2_2.push(m);
    --m;
    arr_jac2_2.push(m);
    p = center_needle + 1;
    arr_jac2_2.push(p);
    p += 11;
    arr_jac2_1.push(p);
    ++p;
    arr_jac2_1.push(p);
    p += 11;
    arr_jac2_2.push(p);
    ++p;
    arr_jac2_2.push(p);
})();
arr_jac2 = [...new Set([...arr_jac2_1, ...arr_jac2_2])];
// func for second jac knit (pt 2)
function jacKnit_2() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (array.includes(s) === true) {
                k.knit('+', bed_1 + s, carrier);
            }
            if (array.includes(s) !== true) {
                k.knit('+', bed_2 + s, carrier);
            }
        }
    } else {    
        for (let s = right_needle; s >= left_needle; --s) {
            if (array.includes(s) === true) {
                k.knit('-', bed_1 + s, carrier);
            }
            if (array.includes(s) !== true) {
                k.knit('-', bed_2 + s, carrier);
            }
        }
    }
}
// func for second jacquard pattern
function jacPattern_2() {
    if (array === arr_jac2) {
        final_rep = 5;
    } else {
        final_rep = 3;
    }
    for (rep = 0; rep < final_rep; ++rep) {
        if (rep > 0) {
            carrier = carrier_4;
            pos_dir_k = true;
            bed_1 = 'b';
            bed_2 = 'f';
            jacKnit_2();
        }
        if (rep > 0 || array === arr_jac2_1) {
            carrier = carrier_4;
            pos_dir_k = false;
            bed_1 = 'b';
            bed_2 = 'f';
            jacKnit_2();
            carrier = carrier_5;
            pos_dir_k = true;
            bed_1 = 'f';
            bed_2 = 'b';
            jacKnit_2();
        }
        if (rep < (final_rep - 1) || array === arr_jac2_2) {
            carrier = carrier_5;
            pos_dir_k = false;
            bed_1 = 'f';
            bed_2 = 'b';
            jacKnit_2();
        }
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// LN 4941-4995
patDivider();

// begin jacquard pattern

// LN 4999-5006 //>>kn-tu (knit allN F, tu all B)
carrier = carrier_4;
k.rack(0.5);
knitFTuckBPos1();

// LN 5009-5017 //<<kn-kn
k.rack(0);
knitFrontNeg1();

// LN 5026-5030 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5036-5040 //<<kn-kn
pos_dir_k = false;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5046-5050 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
end_needle = 'b';
mid_needle = 'f';
knitEndNElseMid();

// LN 5056-5060 //<<kn-kn
pos_dir_k = false;
end_needle = 'b';
mid_needle = 'f';
knitEndNElseMid();

// LN 5066-5070 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5076-5080 //<<kn-kn
pos_dir_k = false;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5086-5090 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5096-5100 //<<kn-kn
arr_jac_1.splice(3, 0, (arr_jac_1[3] - 1));
pos_dir_k = false;
jacKnit_1();


// LN 5106-5110 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5116-5120 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 3, (arr_jac_1[2] + 1));
remove(arr_jac_1, 6, 1);
insert(arr_jac_1, 8, (arr_jac_1[8] - 1));
remove(arr_jac_1, 17, 3);
insert(arr_jac_1, 19, (arr_jac_1[19] - 1));
remove(arr_jac_1, 21, 1);
insert(arr_jac_1, 23, (arr_jac_1[22] + 1), (arr_jac_1[22] + 2), (arr_jac_1[22] + 3), (arr_jac_1[22] + 4));
remove(arr_jac_1, 33, 3);
jacKnit_1();

// LN 5126-5130 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5136-5140 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 5, 1);
insert(arr_jac_1, 7, (arr_jac_1[7] - 1));
insert(arr_jac_1, 19, (arr_jac_1[19] - 1));
remove(arr_jac_1, 22, 1);
remove(arr_jac_1, 28, 1);
jacKnit_1();

// LN 5146-5150 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5156-5160 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 4, 1);
insert(arr_jac_1, 6, (arr_jac_1[6] - 1));
insert(arr_jac_1, 17, (arr_jac_1[16] + 1), (arr_jac_1[16] + 2), (arr_jac_1[16] + 3));
remove(arr_jac_1, 27, 2);
insert(arr_jac_1, 33, (arr_jac_1[32] + 1), (arr_jac_1[32] + 2), (arr_jac_1[32] + 3));
jacKnit_1();

// LN 5166-5170 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5176-5180 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 4, (arr_jac_1[3] + 1));
insert(arr_jac_1, 7, (arr_jac_1[7] - 1));
remove(arr_jac_1, 10, 1);
remove(arr_jac_1, 18, 3);
remove(arr_jac_1, 20, 2);
insert(arr_jac_1, 21, (arr_jac_1[20] + 1));
remove(arr_jac_1, 30, 3);
jacKnit_1();

// LN 5186-5190 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5196-5200 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 5, (arr_jac_1[4] + 1));
remove(arr_jac_1, 10, 1);
remove(arr_jac_1, 22, 1);
insert(arr_jac_1, 23, (arr_jac_1[22] + 1), (arr_jac_1[22] + 2));
remove(arr_jac_1, 26, 1);
jacKnit_1();

// LN 5206-5210 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5216-5220 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 3, 1);
insert(arr_jac_1, 5, (arr_jac_1[4] + 1));
remove(arr_jac_1, 9, 1);
insert(arr_jac_1, 13, (arr_jac_1[13] - 2), (arr_jac_1[13] - 1));
insert(arr_jac_1, 17, (arr_jac_1[16] + 1), (arr_jac_1[16] + 2));
insert(arr_jac_1, 21, (arr_jac_1[20] + 1), (arr_jac_1[20] + 2), (arr_jac_1[20] + 3));
insert(arr_jac_1, 26, (arr_jac_1[26] - 1));
insert(arr_jac_1, 33, (arr_jac_1[33] - 2), (arr_jac_1[33] - 1));
insert(arr_jac_1, 37, (arr_jac_1[36] + 1), (arr_jac_1[36] + 2));
insert(arr_jac_1, 41, (arr_jac_1[40] + 1), (arr_jac_1[40] + 2), (arr_jac_1[40] + 3));
jacKnit_1();

// LN 5226-5230 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5236-5240 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 3, 1);
remove(arr_jac_1, 7, 1);
insert(arr_jac_1, 24, (arr_jac_1[24] - 1));
remove(arr_jac_1, 27, 1);
remove(arr_jac_1, 27, 1);
remove(arr_jac_1, 29, 1);
jacKnit_1();

// LN 5246-5250 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5256-5370 //<<kn-kn //>>kn-kn
for (rep = 0; rep < 3; ++rep) {
    if (rep > 0) {
        carrier = carrier_5;
        pos_dir_k = true;
        end_needle = 'f';
        mid_needle = 'b';
        knitEndNElseMid();
        pos_dir_k = false;
        knitEndNElseMid();
        carrier = carrier_4;
        pos_dir_k = true;
        end_needle = 'b';
        mid_needle = 'f';
        knitEndNElseMid();
    }
    if (rep < 2) {
        carrier = carrier_4;
        pos_dir_k = false;
        end_needle = 'b';
        mid_needle = 'f';
        knitEndNElseMid();
    }
}

// LN 5378-5472
array = arr_jac2_1;
jacPattern_2();

// LN 5478-5631
array = arr_jac2;
jacPattern_2();

// LN 5638-5732
array = arr_jac2_2;
jacPattern_2();
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_2();

k.write('kniterate.k');