// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on (back(?)) bed(s) in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}
console.warn('what do those pink lines mean?');
////////////////
// lace funcs //
////////////////
// generate base array for 1st sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE - immediately invoked function expression)
let arr_lace_1 = [];
(function arrLaceRange_1() {
  for (let i = -1; i <= 252; i += 6) {
    if (i >= left_needle && i <= right_needle) {
        arr_lace_1.push(i);
    }
  }
})();
// generate base array for 2nd sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_lace_2 = [];
(function arrLaceRange_2() {
    let i = 0;
    while (i <= 252) {
        i += 4;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_2.push(i);
        }
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_2.push(i);
        }
    }
})();
// generate base array for 3rd sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_lace_3 = [];
(function arrLaceRange_3() {
    let i = 1;
    while (i <= 252) {
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_3.push(i);
        }
        i += 4;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_3.push(i);
        }
    }
})();
// func for all pos dir transfers in lace diamond pattern
function laceXferPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (pos_back_xfer) {
            if (arr_lace_1.includes(s) === true && (s + fs) != left_needle) {
                k.xfer('b' + (s + bs), 'f' + (s + fs));
            }
        } else {
            if (arr_lace_1.includes(s) === true) {
                k.xfer('f' + (s + fs), 'b' + (s + bs));
            }
        }

    }
}
// func for all neg dir transfer in lace diamond pattern
function laceXferNeg_1() {
    for (let s = right_needle; s >= left_needle; --s) {
        if (neg_front_xfer) {
            if (arr_lace_1.includes(s) === true && (s + fs) != left_needle) {
                k.xfer('f' + (s + fs), 'b' + (s + bs));
            }
        } else {
            if (arr_lace_1.includes(s) === true && (s + bs) != right_needle) {
                k.xfer('b' + (s + bs), 'f' + (s + fs));
            }
        }
    }
}
console.warn('what do those pink lines mean?');
// func for 2nd neg dir transfer in lace diamond pattern
function laceXferNeg_2() {
    first_start ? startN = right_needle : startN = (right_needle - 3);
    first_end ? (endN = (left_needle + 2), array = arr_lace_2) : (endN = left_needle, array = arr_lace_3);
    for (let s = startN; s >= endN; --s) {
        if (array.includes(s) === true) {
            k.xfer('f' + s, 'b' + s);
        }
    }
}
// func for missing on both beds in pos dir, once
function missAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        // k.miss('+', 'f' + s, carrier);
        k.miss('+', 'b' + s, carrier);
    }
}
// func for lace pattern repeat
function lacePattern() {
// LN 1759-1763
    neg_front_xfer = true;
    fs = 0;
    bs = 0;
    laceXferNeg_1();
// LN 1769-1777
    pos_back_xfer = true;
    k.rack(1);
    bs = 0;
    fs = 1;
    laceXferPos();
    k.rack(0);
// LN 1782-1796
    knitFrontNeg1();
    knitFrontPos1();
// LN 1802-1806
    first_start = true;
    first_end = true;
    laceXferNeg_2();
// LN 1812-1817
    k.rack(-1);
    bs = 5;
    fs = 4;
    laceXferPos();
// LN 1823-1828
    neg_front_xfer = false;
    k.rack(1);
    bs = 1;
    fs = 2;
    laceXferNeg_1();
// LN 1835-1841
    missAllPos1();
    k.rack(0);
// LN 1846-1860
    knitFrontNeg1();
    knitFrontPos1();
// LN 1867-1871
    first_end = false;
    laceXferNeg_2();
// LN 1879-1884
    k.rack(-1);
    bs = 4;
    fs = 3;
    laceXferPos();
// LN 1890-1895
    k.rack(1);
    bs = 2;
    fs = 3;
    laceXferNeg_1();
// LN 1902-1908
    missAllPos1();
    k.rack(0);
// LN 1913-1927
    knitFrontNeg1();
// ^ line above is last function executed in last repeat (repeats 3 times all way thru (include 1st), and once partially thru. total = 4)
    if (lace_rep !== (last_lace_rep - 1)) {
        knitFrontPos1();
    // LN 1933-1937
        neg_front_xfer = true;
        fs = 3;
        bs = 3;
        laceXferNeg_1();
    // LN 1943-1951
        k.rack(-1);
        bs = 3;
        fs = 2;
        laceXferPos();
        k.rack(0);
    // LN 1956-1970
        knitFrontNeg1();
        knitFrontPos1();
    // LN 1976-1980
        first_start = false;
        laceXferNeg_2();
    // LN 1986-1991
        k.rack(-1);
        bs = 2;
        fs = 1;
        laceXferPos();
    // LN 1997-2002
        k.rack(1);
        neg_front_xfer = false;
        bs = 5;
        fs = 4;
        laceXferNeg_1();
    // LN 2009-2015
        missAllPos1();
        k.rack(0);
    // LN 2020-2034
        knitFrontNeg1();
        knitFrontPos1();
    // LN 2040-2044
        first_end = true;
        laceXferNeg_2();
    // LN 2050-2055
        k.rack(-1);
        bs = 1;
        fs = 0;
        laceXferPos();
    // LN 2061-2066
        k.rack(1);
        bs = 5;
        fs = 6;
        laceXferNeg_1();
    // LN 2073-2079
        missAllPos1();
        k.rack(0);
    // LN 2084-2098
        knitFrontNeg1();
        knitFrontPos1();
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// begin lace diamond pattern

// LN 1749-1753
carrier = carrier_4;
knitFrontPos1();

// LN 1759-2957
let last_lace_rep = 4;
for (lace_rep = 0; lace_rep < last_lace_rep; ++lace_rep) {
    lacePattern();
}
// LN 1759-2098 = 1st repeat
// LN 2101-2447 = 2nd repeat
// LN 2453-2792 = 3rd repeat
// LN 2798-2957 = 4th & final repeat (partial repeat)

// LN 2963-3017
patDivider();

k.write('kniterate.k');