// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
const center_needle = Math.round(((left_needle - (right_needle - 1))/2) + right_needle);
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on back bed in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}
// func for tuck on alternate needles switching btw front & back bed, starting in pos dir
function tuckAltNPos() {
    if (alt_tuck) {
        tuck_needle = 'f';
        knit_needle = 'b';
    } else {
        tuck_needle = 'b';
        knit_needle = 'f';
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.tuck('+', tuck_needle + s, carrier);
        } else {
            k.knit('+', knit_needle + s, carrier);
        }
    }
    // if (alt_tuck) {
    //     tuck_needle = 'b';
    //     knit_needle = 'f';
    // }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 !== 0) {
            k.knit('-', 'f' + s, carrier);
        } else {
            k.tuck('-', 'b' + s, carrier);
        }
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// LN 4401-4455
patDivider();

// begin waffle pattern

// LN 4461-4465 //>>Tr-Rr
neg_dir_x = false;
alt_pos = true;
give_needle = 'f';
receive_needle = 'b';
xferAltN();

// LN 4471-4475
missAllNeg1();

// LN 4481-4915 //>>Kn-Tu, <<Kn-Tu, >>Kn-Kn, <<Kn-Kn
carrier = carrier_4;
alt_tuck = false;
alt_pos = false;
for (waffle_rep = 0; waffle_rep < 11; ++waffle_rep) {
// LN 4481-4495
    tuckAltNPos();
// LN 4501-4515
    knitAltNPos();
}

// LN 4921-4925 //>>Rr-Tr
neg_dir_x = false;
alt_pos = true;
give_needle = 'b';
receive_needle = 'f';
xferAltN();

// LN 4931-4935
missAllNeg1();

// LN 4941-4995
patDivider();

k.write('kniterate.k');