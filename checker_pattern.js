// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on (back(?)) bed(s) in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
///////////////////
// checker funcs //
///////////////////
// func for 1st neg dir transfer in checker pattern
function checkerXferNeg_1() {
    let xfer_rightN = right_needle;
    if (arr_checker_1.indexOf(xfer_rightN) === -1) {
        arr_checker_1.push(xfer_rightN);
    }
    k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
//loop
    while (xfer_rightN > left_needle) {
        xfer_rightN -= 8;
        if (arr_checker_1.indexOf(xfer_rightN) === -1) {
            arr_checker_1.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        xfer_rightN -= 2;
        if (arr_checker_1.indexOf(xfer_rightN) === -1) {
            arr_checker_1.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        if ((xfer_rightN - 2) > (left_needle + 1)) {
            xfer_rightN -= 2;
            if (arr_checker_1.indexOf(xfer_rightN) === -1) {
                arr_checker_1.push(xfer_rightN);
            }
            k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);   
        } else {
            break;
        }
    }
}
// func for 1st pos dir transfer in checker pattern
function checkerXferPos_1() {
    let xfer_leftN = left_needle;
    if (arr_checker_1.indexOf(xfer_leftN) === -1) {
        arr_checker_1.push(xfer_leftN);
    }
    k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
    while (xfer_leftN < right_needle) {
        xfer_leftN += 2;
        if (arr_checker_1.indexOf(xfer_leftN) === -1) {
            arr_checker_1.push(xfer_leftN);
        }
        k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        xfer_leftN += 8;
        if (arr_checker_1.indexOf(xfer_leftN) === -1) {
            arr_checker_1.push(xfer_leftN);
        }
        k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        if ((xfer_leftN + 2) < (right_needle - 1)) {
            xfer_leftN += 2;
            if (arr_checker_1.indexOf(xfer_leftN) === -1) {
                arr_checker_1.push(xfer_leftN);
            }
            k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        } else {
            break;
        }
    }
}
// func for 1st knitting transfer in checker pattern starting in neg dir
function checkerKnitNeg_1() {
    for (let r = 0; r < 8; r += 2) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_checker_1.includes(s) === true) {
                k.knit('-', 'b' + s, carrier);
            } else {
                k.knit('-', 'f' + s, carrier);
            }    
        }
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_checker_1.includes(s) === true) {
                k.knit('+', 'b' + s, carrier);
            } else {
                k.knit('+', 'f' + s, carrier);
            }    
        }
    }
}
// func for 2nd neg dir transfer in checker pattern
function checkerXferNeg_2() {
    xfer_rightN = arr_checker_1[1] - 1;
    if (arr_checker_2.indexOf(xfer_rightN) === -1) {
        arr_checker_2.push(xfer_rightN);
    }
    k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
//loop
    while (xfer_rightN > left_needle) {
        xfer_rightN -= 2;
        if (arr_checker_2.indexOf(xfer_rightN) === -1) {
            arr_checker_2.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        xfer_rightN -= 2;
        if (arr_checker_2.indexOf(xfer_rightN) === -1) {
            arr_checker_2.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN); 
        if ((xfer_rightN - 8) > (left_needle + 1)) {
            xfer_rightN -= 8;
            if (arr_checker_2.indexOf(xfer_rightN) === -1) {
                arr_checker_2.push(xfer_rightN);
            }
            k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        } else {
            break;
        }
    }
}
console.warn('NOTE TO SELF: not sure if I need to put these values in the array yet, so commented out for now');
console.warn('NOTE TO SELF: also need to determine if need left_needle +1 or just left_needle, and for other xfer checker funcs as well');
// func for 2nd pos dir transfer in checker pattern
function checkerXferPos_2() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_checker_1.includes(s) === false) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}
// func for 2nd knitting transfer in checker pattern starting in neg dir
function checkerKnitNeg_2() {
    if ((checker_rep + 1) < last_checker_rep) {
        last_row = 8;
    } else {
        last_row = 7;
    }
    let r = 0;
    while (r < last_row) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_checker_1.includes(s) === false) {
                k.knit('-', 'b' + s, carrier);
            } else {
                k.knit('-', 'f' + s, carrier);
            }    
        }
        ++r;
        if (r < last_row) {
            for (let s = left_needle; s <= right_needle; ++s) {
                if (arr_checker_1.includes(s) === false) {
                    k.knit('+', 'b' + s, carrier);
                } else {
                    k.knit('+', 'f' + s, carrier);
                }    
            }
            ++r;
        } else {
            break;
        }
    }
}
// func for checker pattern repeat
function checkerPattern() {
    arr_checker_1 = [];
    give_needle = 'f';
    receive_needle = 'b';
    checkerXferNeg_1();
    checkerXferPos_1();
    arr_checker_1.sort((a, b) => b - a);
    give_needle = 'b';
    receive_needle = 'f';
    if (checker_rep > 0) {
        checkerXferNeg_2();
        checkerXferPos_2();
    }
    checkerKnitNeg_1();
    checkerXferNeg_1();
    checkerXferPos_1();
    arr_checker_2 = [];
    give_needle = 'f';
    receive_needle = 'b';
    checkerXferNeg_2();
    checkerXferPos_2();
    checkerKnitNeg_2();
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
// miss to get carriage back to left side ... should i miss on front, back, or both? (?)
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// begin checker pattern

// LN 1196-1200
carrier = carrier_4;
knitFrontPos1();

// LN 1207-1659
// repeat checker pattern
let last_checker_rep = 2;
for (checker_rep = 0; checker_rep < last_checker_rep; ++checker_rep) {
    checkerPattern();
}
// LN 1666-1680
if (checker_rep === last_checker_rep) {
    give_needle = 'b';
    receive_needle = 'f';
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_checker_2.includes(s) === true) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
    for (let s = right_needle; s>= left_needle; --s) {
        if (arr_checker_1.includes(s) === false) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}

// LN 1686-1740
patDivider();

k.write('kniterate.k');