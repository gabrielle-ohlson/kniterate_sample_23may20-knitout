// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
const center_needle = Math.round(((left_needle - (right_needle - 1))/2) + right_needle);
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on back bed in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
///////////////////
// checker funcs //
///////////////////
// func for 1st neg dir transfer in checker pattern
function checkerXferNeg_1() {
    let xfer_rightN = right_needle;
    if (arr_checker_1.indexOf(xfer_rightN) === -1) {
        arr_checker_1.push(xfer_rightN);
    }
    k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
//loop
    while (xfer_rightN > left_needle) {
        xfer_rightN -= 8;
        if (arr_checker_1.indexOf(xfer_rightN) === -1) {
            arr_checker_1.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        xfer_rightN -= 2;
        if (arr_checker_1.indexOf(xfer_rightN) === -1) {
            arr_checker_1.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        if ((xfer_rightN - 2) > (left_needle + 1)) {
            xfer_rightN -= 2;
            if (arr_checker_1.indexOf(xfer_rightN) === -1) {
                arr_checker_1.push(xfer_rightN);
            }
            k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);   
        } else {
            break;
        }
    }
}
// func for 1st pos dir transfer in checker pattern
function checkerXferPos_1() {
    let xfer_leftN = left_needle;
    if (arr_checker_1.indexOf(xfer_leftN) === -1) {
        arr_checker_1.push(xfer_leftN);
    }
    k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
    while (xfer_leftN < right_needle) {
        xfer_leftN += 2;
        if (arr_checker_1.indexOf(xfer_leftN) === -1) {
            arr_checker_1.push(xfer_leftN);
        }
        k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        xfer_leftN += 8;
        if (arr_checker_1.indexOf(xfer_leftN) === -1) {
            arr_checker_1.push(xfer_leftN);
        }
        k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        if ((xfer_leftN + 2) < (right_needle - 1)) {
            xfer_leftN += 2;
            if (arr_checker_1.indexOf(xfer_leftN) === -1) {
                arr_checker_1.push(xfer_leftN);
            }
            k.xfer(give_needle + xfer_leftN, receive_needle + xfer_leftN);
        } else {
            break;
        }
    }
}
// func for 1st knitting transfer in checker pattern starting in neg dir
function checkerKnitNeg_1() {
    for (let r = 0; r < 8; r += 2) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_checker_1.includes(s) === true) {
                k.knit('-', 'b' + s, carrier);
            } else {
                k.knit('-', 'f' + s, carrier);
            }    
        }
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_checker_1.includes(s) === true) {
                k.knit('+', 'b' + s, carrier);
            } else {
                k.knit('+', 'f' + s, carrier);
            }    
        }
    }
}
// func for 2nd neg dir transfer in checker pattern
function checkerXferNeg_2() {
    xfer_rightN = arr_checker_1[1] - 1;
    if (arr_checker_2.indexOf(xfer_rightN) === -1) {
        arr_checker_2.push(xfer_rightN);
    }
    k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
//loop
    while (xfer_rightN > left_needle) {
        xfer_rightN -= 2;
        if (arr_checker_2.indexOf(xfer_rightN) === -1) {
            arr_checker_2.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        xfer_rightN -= 2;
        if (arr_checker_2.indexOf(xfer_rightN) === -1) {
            arr_checker_2.push(xfer_rightN);
        }
        k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN); 
        if ((xfer_rightN - 8) > (left_needle + 1)) {
            xfer_rightN -= 8;
            if (arr_checker_2.indexOf(xfer_rightN) === -1) {
                arr_checker_2.push(xfer_rightN);
            }
            k.xfer(give_needle + xfer_rightN, receive_needle + xfer_rightN);
        } else {
            break;
        }
    }
}
console.log('NOTE TO SELF: need to determine if need left_needle +1 or just left_needle, and for other xfer checker funcs as well');
// func for 2nd pos dir transfer in checker pattern
function checkerXferPos_2() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_checker_1.includes(s) === false) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}
// func for 2nd knitting transfer in checker pattern starting in neg dir
function checkerKnitNeg_2() {
    if ((checker_rep + 1) < last_checker_rep) {
        last_row = 8;
    } else {
        last_row = 7;
    }
    let r = 0;
    while (r < last_row) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_checker_1.includes(s) === false) {
                k.knit('-', 'b' + s, carrier);
            } else {
                k.knit('-', 'f' + s, carrier);
            }    
        }
        ++r;
        if (r < last_row) {
            for (let s = left_needle; s <= right_needle; ++s) {
                if (arr_checker_1.includes(s) === false) {
                    k.knit('+', 'b' + s, carrier);
                } else {
                    k.knit('+', 'f' + s, carrier);
                }    
            }
            ++r;
        } else {
            break;
        }
    }
}
// func for checker pattern repeat
function checkerPattern() {
    arr_checker_1 = [];
    give_needle = 'f';
    receive_needle = 'b';
    checkerXferNeg_1();
    checkerXferPos_1();
    arr_checker_1.sort((a, b) => b - a);
    give_needle = 'b';
    receive_needle = 'f';
    if (checker_rep > 0) {
        checkerXferNeg_2();
        checkerXferPos_2();
    }
    checkerKnitNeg_1();
    checkerXferNeg_1();
    checkerXferPos_1();
    arr_checker_2 = [];
    give_needle = 'f';
    receive_needle = 'b';
    checkerXferNeg_2();
    checkerXferPos_2();
    checkerKnitNeg_2();
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}
////////////////
// lace funcs //
////////////////
// generate base array for 1st sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE - immediately invoked function expression)
let arr_lace_1 = [];
(function arrLaceRange_1() {
  for (let i = -1; i <= 252; i += 6) {
    if (i >= left_needle && i <= right_needle) {
        arr_lace_1.push(i);
    }
  }
})();
// generate base array for 2nd sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_lace_2 = [];
(function arrLaceRange_2() {
    let i = 0;
    while (i <= 252) {
        i += 4;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_2.push(i);
        }
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_2.push(i);
        }
    }
})();
// generate base array for 3rd sequence in lace pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_lace_3 = [];
(function arrLaceRange_3() {
    let i = 1;
    while (i <= 252) {
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_3.push(i);
        }
        i += 4;
        if (i >= left_needle && i <= right_needle) {
            arr_lace_3.push(i);
        }
    }
})();
// func for all pos dir transfers in lace diamond pattern
function laceXferPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (pos_back_xfer) {
            if (arr_lace_1.includes(s) === true && (s + fs) != left_needle) {
                k.xfer('b' + (s + bs), 'f' + (s + fs));
            }
        } else {
            if (arr_lace_1.includes(s) === true) {
                k.xfer('f' + (s + fs), 'b' + (s + bs));
            }
        }

    }
}
// func for all neg dir transfer in lace diamond pattern
function laceXferNeg_1() {
    for (let s = right_needle; s >= left_needle; --s) {
        if (neg_front_xfer) {
            if (arr_lace_1.includes(s) === true && (s + fs) != left_needle) {
                k.xfer('f' + (s + fs), 'b' + (s + bs));
            }
        } else {
            if (arr_lace_1.includes(s) === true && (s + bs) != right_needle) {
                k.xfer('b' + (s + bs), 'f' + (s + fs));
            }
        }
    }
}
console.warn('what do those pink lines mean?');
// func for 2nd neg dir transfer in lace diamond pattern
function laceXferNeg_2() {
    first_start ? startN = right_needle : startN = (right_needle - 3);
    first_end ? (endN = (left_needle + 2), array = arr_lace_2) : (endN = left_needle, array = arr_lace_3);
    for (let s = startN; s >= endN; --s) {
        if (array.includes(s) === true) {
            k.xfer('f' + s, 'b' + s);
        }
    }
}
// func for missing on both beds in pos dir, once
function missAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        // k.miss('+', 'f' + s, carrier);
        k.miss('+', 'b' + s, carrier);
    }
}
// func for lace pattern repeat
function lacePattern() {
// LN 1759-1763
    neg_front_xfer = true;
    fs = 0;
    bs = 0;
    laceXferNeg_1();
// LN 1769-1777
    pos_back_xfer = true;
    k.rack(1);
    bs = 0;
    fs = 1;
    laceXferPos();
    k.rack(0);
// LN 1782-1796
    knitFrontNeg1();
    knitFrontPos1();
// LN 1802-1806
    first_start = true;
    first_end = true;
    laceXferNeg_2();
// LN 1812-1817
    k.rack(-1);
    bs = 5;
    fs = 4;
    laceXferPos();
// LN 1823-1828
    neg_front_xfer = false;
    k.rack(1);
    bs = 1;
    fs = 2;
    laceXferNeg_1();
// LN 1835-1841
    missAllPos1();
    k.rack(0);
// LN 1846-1860
    knitFrontNeg1();
    knitFrontPos1();
// LN 1867-1871
    first_end = false;
    laceXferNeg_2();
// LN 1879-1884
    k.rack(-1);
    bs = 4;
    fs = 3;
    laceXferPos();
// LN 1890-1895
    k.rack(1);
    bs = 2;
    fs = 3;
    laceXferNeg_1();
// LN 1902-1908
    missAllPos1();
    k.rack(0);
// LN 1913-1927
    knitFrontNeg1();
// ^ line above is last function executed in last repeat (repeats 3 times all way thru (include 1st), and once partially thru. total = 4)
    if (lace_rep !== (last_lace_rep - 1)) {
        knitFrontPos1();
    // LN 1933-1937
        neg_front_xfer = true;
        fs = 3;
        bs = 3;
        laceXferNeg_1();
    // LN 1943-1951
        k.rack(-1);
        bs = 3;
        fs = 2;
        laceXferPos();
        k.rack(0);
    // LN 1956-1970
        knitFrontNeg1();
        knitFrontPos1();
    // LN 1976-1980
        first_start = false;
        laceXferNeg_2();
    // LN 1986-1991
        k.rack(-1);
        bs = 2;
        fs = 1;
        laceXferPos();
    // LN 1997-2002
        k.rack(1);
        neg_front_xfer = false;
        bs = 5;
        fs = 4;
        laceXferNeg_1();
    // LN 2009-2015
        missAllPos1();
        k.rack(0);
    // LN 2020-2034
        knitFrontNeg1();
        knitFrontPos1();
    // LN 2040-2044
        first_end = true;
        laceXferNeg_2();
    // LN 2050-2055
        k.rack(-1);
        bs = 1;
        fs = 0;
        laceXferPos();
    // LN 2061-2066
        k.rack(1);
        bs = 5;
        fs = 6;
        laceXferNeg_1();
    // LN 2073-2079
        missAllPos1();
        k.rack(0);
    // LN 2084-2098
        knitFrontNeg1();
        knitFrontPos1();
    }
}
//////////////////
// 2x2Rib funcs //
//////////////////
// generate base array for 1st xfer sequence in 2x2Rib pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_x_1 = [];
(function arr2RibXRange_1() {
    let i = 1;
    while (i <= 252) {
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_2rib_x_1.push(i);
        }
        i += 5;
        if (i >= left_needle && i <= right_needle) {
            arr_2rib_x_1.push(i);
        }
    }
})();
// generate base array for 2nd xfer sequence in 2x2Rib pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_x_2 = [];
(function arr2RibXRange_2() {
  for (let i = -5; i <= 252; i += 7) {
    if (i >= left_needle && i <= right_needle) {
        arr_2rib_x_2.push(i);
    }
  }
})();
// generate base array for 1st knit sequence in 2x2Rib pat, including whole needle bed (then in func, altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_k_1 = [];
(function arr2RibKRange_1() {
    for (let i = -3; i <= 252; i += 4) {
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        if (i >= 252) {
            break;
        }
    }
})();
// generate base array for 2st knit sequence in 2x2Rib pat, including whole needle bed (then in func, altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_k_2 = [];
(function arr2RibKRange_2() {
    for (let i = -3; i <= 252; i += 6) {
        arr_2rib_k_2.push(i);
        ++i;
        arr_2rib_k_2.push(i);
        if (i >= 252) {
            break;
        }
    }
})();
// func for pos dir xfer in 2x2Rib pat
function rib2x2Xfer_1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_2rib_x_1.includes(s) === true) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}
// func for neg dir xfer in 2x2Rib pat
function rib2x2Xfer_2() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_x_2.includes(s) === true && (s + gs) < right_needle) {
                k.xfer(give_needle + (s + gs), receive_needle + (s + rs));
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_x_2.includes(s) === true && (s + gs) <= right_needle) {
                k.xfer(give_needle + (s + gs), receive_needle + (s + rs));
            }            
        }
    }
}
// 1st func for knitting 2x2Rib, can toggle btw pos & neg dir
function rib2x2Knit_1() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_k_1.includes(s) === true) {
                k.knit('+', 'f' + s, carrier);
            } else {
                k.knit('+', 'b' + s, carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_k_1.includes(s) === true) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }            
        }
    }
}
// 2nd func for knitting 2x2Rib, can toggle btw pos & neg dir
function rib2x2Knit_2() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_k_2.includes(s) === true) {
                k.knit('+', 'f' + (s + fs), carrier);
            }
            if (arr_2rib_k_1.includes(s) !== true) {
                k.knit('+', 'b' + (s + bs), carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_k_2.includes(s) === true && (s + fs) <= right_needle) {
                k.knit('-', 'f' + (s + fs), carrier);
            }
            if (arr_2rib_k_1.includes(s) !== true && (s + bs) <= right_needle) {
                k.knit('-', 'b' + (s + bs), carrier);
            }
        }
    }
}
// func for 2x2rib pattern repeat
function rib2x2Pattern() {
// LN 3048-3091 (first time around)
    carrier = carrier_4;
    for (let r = 0; r < 6; r += 2) {
        pos_dir_k = true;
        rib2x2Knit_1();
        pos_dir_k = false;
        rib2x2Knit_1();
    }
    if (rib_rep !== (last_rib_rep - 1)) {
    // 3096-3100
        pos_dir_k = true;
        fs = 0;
        bs = 0;
        rib2x2Knit_2();
    // LN 3104-3108
        give_needle = 'f';
        receive_needle = 'b';
        gs = 2; // could alternatively be -5
        rs = 2;
        rib2x2Xfer_2();
    // LN 3112-3116
        neg_dir_x = false;
        gs = 3;
        rs = 3;
        rib2x2Xfer_2();
    // LN 3120-3124 //knit in neg dir once
        pos_dir_k = false;
        fs = 2;
        bs = 0;
        rib2x2Knit_2();
    // LN 3128-2132 //F xfer pos dir
        gs = 4;
        rs = 4;
        rib2x2Xfer_2();
    // LN 3136-3140 //F xfer neg dir
        neg_dir_x = true;
        gs = 5;
        rs = 5;
        rib2x2Xfer_2();
    // LN 3143-3150 //B xfer pos dir
        neg_dir_x = false;
        k.rack(-2);
        give_needle = 'b';
        receive_needle = 'f';
        gs = 4; // could alternatively be -3
        rs = 2; // could alternatively be -5
        rib2x2Xfer_2();
    // LN 3154-3158 //B xfer neg dir
        neg_dir_x = true;
        gs = 5;
        rs = 3;
        rib2x2Xfer_2();
    // LN 3161-3169 //B xfer pos dir
        neg_dir_x = false;
        k.rack(2);
        gs = 2;
        rs = 4;
        rib2x2Xfer_2();
    // LN 3173-3180 //B xfer neg dir
        neg_dir_x = true;
        gs = 3;
        rs = 5;
        rib2x2Xfer_2();
        k.rack(0);
    } else {
    // LN 3778-3782 //B xfer pos dir
        give_needle = 'b';
        receive_needle = 'f';
        rib2x2Xfer_1();
    // LN 3788-3792 //B xfer neg dir
        neg_dir_x = true;
        give_needle = 'b';
        receive_needle = 'f';
        gs = 0;
        rs = 0;
        rib2x2Xfer_2();
    }
}
/////////////////////////
// full cardigan funcs //
/////////////////////////
// func for tuck on alternate needles switching btw front & back bed, starting in pos dir
function tuckAltNPos() {
    if (alt_tuck) {
        tuck_needle = 'f';
        knit_needle = 'b';
    } else {
        tuck_needle = 'b';
        knit_needle = 'f';
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.tuck('+', tuck_needle + s, carrier);
        } else {
            k.knit('+', knit_needle + s, carrier);
        }
    }
    // if (alt_tuck) {
    //     tuck_needle = 'b';
    //     knit_needle = 'f';
    // }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 !== 0) {
            k.knit('-', 'f' + s, carrier);
        } else {
            k.tuck('-', 'b' + s, carrier);
        }
    }
}
////////////////////
// jacquard funcs //
////////////////////
// func for knitting all needles on front bed & tucking on all N on back bed in pos dir, once
function knitFTuckBPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.tuck('+', 'b' + s, carrier);
    }
}
// func for knitting end needles on one bed and all other needles on other bed
function knitEndNElseMid() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (s === left_needle || s === right_needle) {
                k.knit('+', end_needle + s, carrier);
            } else {
                k.knit('+', mid_needle + s, carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s === right_needle || s === left_needle) {
                k.knit('-', end_needle + s, carrier);
            } else {
                k.knit('-', mid_needle + s, carrier);
            }
        }
    }
}
// funcs for pushing selected needles to jacquard arrays with variable number of misses in between
function arrJacPush_1(number) {
    i += number;
    array.push(i);
    ++i;
    array.push(i);
}
function arrJacPush_2(number) {
    i += number;
    for (let p = 0; p < 5; ++p) {
        array.push(i);
        if (p < 4) {
            ++i;
        }
    }
}
// generate base array for 1st knit sequence in jacquard pat, altered according to left/right-most needle variables so can be reused (IIFE)
let arr_jac_1 = [];
(function arrJacRange() {
    i = left_needle;
    array = arr_jac_1;
    array.push(i);
    arrJacPush_1(8);
    arrJacPush_1(3);
    arrJacPush_1(4);
    arrJacPush_1(5);
    arrJacPush_1(5);
    arrJacPush_1(7);
    arrJacPush_2(6);
    arrJacPush_1(5);
    arrJacPush_1(3);
    arrJacPush_1(5);
    arrJacPush_1(5);
    arrJacPush_1(6);
    arrJacPush_2(6);
    i += 8;
    array.push(i);
})();
// func for first jacquard knit
function jacKnit_1() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_jac_1.includes(s) === true) {
                k.knit('+', bed_1 + s, carrier);
            }
            if (arr_jac_1.includes(s) !== true) {
                k.knit('+', bed_2 + s, carrier);
            }
        }
    } else {    
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_jac_1.includes(s) === true) {
                k.knit('-', bed_1 + s, carrier);
            }
            if (arr_jac_1.includes(s) !== true) {
                k.knit('-', bed_2 + s, carrier);
            }
        }
    }
}
// insert with splice, which alters original array
function insert(array, index, ...items) {
    array.splice(index, 0, ...items);
}
// // insert with slice, which makes new array instead
// function insert(array, index, ...items) {
//     return [
// 		...array.slice(0, index),
// 		...items,
// 		...array.slice(index)
// 	];
// }
// let result = insert(arr_jac_1, 2, (arr_jac_1[2] + 1));
function remove(array, index, delete_count, ...items) {
    array.splice(index, delete_count, ...items);
}
// generate base arrays for 2nd knit sequence in jacquard pat (kind of like a part 2), altered according to left/right-most needle variables so can be reused (IIFE)
let arr_jac2_1 = [];
let arr_jac2_2 = [];
(function arrJac2Range() {
    arr_jac2_1.push(left_needle);
    arr_jac2_1.push(right_needle);
    arr_jac2_2.push(left_needle);
    arr_jac2_2.push(right_needle);
    m = center_needle;
    arr_jac2_2.push(m);
    m -= 11;
    arr_jac2_1.push(m);
    --m;
    arr_jac2_1.push(m);
    m -= 11;
    arr_jac2_2.push(m);
    --m;
    arr_jac2_2.push(m);
    p = center_needle + 1;
    arr_jac2_2.push(p);
    p += 11;
    arr_jac2_1.push(p);
    ++p;
    arr_jac2_1.push(p);
    p += 11;
    arr_jac2_2.push(p);
    ++p;
    arr_jac2_2.push(p);
})();
arr_jac2 = [...new Set([...arr_jac2_1, ...arr_jac2_2])];
// func for second jac knit (pt 2)
function jacKnit_2() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (array.includes(s) === true) {
                k.knit('+', bed_1 + s, carrier);
            }
            if (array.includes(s) !== true) {
                k.knit('+', bed_2 + s, carrier);
            }
        }
    } else {    
        for (let s = right_needle; s >= left_needle; --s) {
            if (array.includes(s) === true) {
                k.knit('-', bed_1 + s, carrier);
            }
            if (array.includes(s) !== true) {
                k.knit('-', bed_2 + s, carrier);
            }
        }
    }
}
// func for second jacquard pattern
function jacPattern_2() {
    if (array === arr_jac2) {
        final_rep = 5;
    } else {
        final_rep = 3;
    }
    for (rep = 0; rep < final_rep; ++rep) {
        if (rep > 0) {
            carrier = carrier_4;
            pos_dir_k = true;
            bed_1 = 'b';
            bed_2 = 'f';
            jacKnit_2();
        }
        if (rep > 0 || array === arr_jac2_1) {
            carrier = carrier_4;
            pos_dir_k = false;
            bed_1 = 'b';
            bed_2 = 'f';
            jacKnit_2();
            carrier = carrier_5;
            pos_dir_k = true;
            bed_1 = 'f';
            bed_2 = 'b';
            jacKnit_2();
        }
        if (rep < (final_rep - 1) || array === arr_jac2_2) {
            carrier = carrier_5;
            pos_dir_k = false;
            bed_1 = 'f';
            bed_2 = 'b';
            jacKnit_2();
        }
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// begin checker pattern

// LN 1196-1200
carrier = carrier_4;
knitFrontPos1();

// LN 1207-1659
// repeat checker pattern
let last_checker_rep = 2;
for (checker_rep = 0; checker_rep < last_checker_rep; ++checker_rep) {
    checkerPattern();
}
// LN 1666-1680
if (checker_rep === last_checker_rep) {
    give_needle = 'b';
    receive_needle = 'f';
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_checker_2.includes(s) === true) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
    for (let s = right_needle; s>= left_needle; --s) {
        if (arr_checker_1.includes(s) === false) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}

// LN 1686-1740
patDivider();

// begin lace diamond pattern

// LN 1749-1753
carrier = carrier_4;
knitFrontPos1();

// start repeat
// LN 1759-2957
let last_lace_rep = 4;
for (lace_rep = 0; lace_rep < last_lace_rep; ++lace_rep) {
    lacePattern();
}
// LN 1759-2098 = 1st repeat
// LN 2101-2447 = 2nd repeat
// LN 2453-2792 = 3rd repeat
// LN 2798-2957 = 4th & final repeat (partial repeat)

// LN 2963-3017
patDivider();

// begin 2x2 cable pattern

// LN 3026-3030
give_needle = 'f';
receive_needle = 'b';
rib2x2Xfer_1();

// LN 3036-2040
neg_dir_x = true;
give_needle = 'f';
receive_needle = 'b';
gs = 0;
rs = 0;
rib2x2Xfer_2();

// start repeat
// LN 3048-3792
let last_rib_rep = 6;
for (rib_rep = 0; rib_rep < last_rib_rep; ++rib_rep) {
    rib2x2Pattern();
}
// LN 3048-3180 = 1st repeat
// LN 3185-3317 = 2nd repeat
// LN 3321-3453 = 3rd repeat
// LN 3457-3589 = 4th repeat
// LN 3593-3724 = 5th repeat
// LN 3728-3792 = 6th & final repeat (partial repeat)

// LN 3798-3852
patDivider();

// begin full cardigan pattern

// LN 3858-3862 //F xfer pos dir
neg_dir_x = false;
give_needle = 'f';
receive_needle = 'b';
xferAltN();

// LN 3868-3872 //miss neg dir
missAllNeg1();

// start repeat
// LN 3381-4375 //>>Tu-Kn, <<Kn-Tu
carrier = carrier_4;
alt_tuck = true;
for (cardigan_rep = 0; cardigan_rep < 25; ++cardigan_rep) {
    tuckAltNPos();
}

// LN 4381-4385 //>>Rr-Tr
neg_dir_x = false;
alt_pos = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();

// Ln 4391-4395
missAllNeg1();

// LN 4401-4455
patDivider();

// begin waffle pattern

// LN 4461-4465 //>>Tr-Rr
neg_dir_x = false;
alt_pos = true;
give_needle = 'f';
receive_needle = 'b';
xferAltN();

// LN 4471-4475
missAllNeg1();

// LN 4481-4915 //>>Kn-Tu, <<Kn-Tu, >>Kn-Kn, <<Kn-Kn
carrier = carrier_4;
alt_tuck = false;
alt_pos = false;
for (waffle_rep = 0; waffle_rep < 11; ++waffle_rep) {
// LN 4481-4495
    tuckAltNPos();
// LN 4501-4515
    knitAltNPos();
}

// LN 4921-4925 //>>Rr-Tr
neg_dir_x = false;
alt_pos = true;
give_needle = 'b';
receive_needle = 'f';
xferAltN();

// LN 4931-4935
missAllNeg1();

// LN 4941-4995
patDivider();

// begin jacquard pattern

// LN 4999-5006 //>>kn-tu (knit allN F, tu all B)
carrier = carrier_4;
k.rack(0.5);
knitFTuckBPos1();

// LN 5009-5017 //<<kn-kn
k.rack(0);
knitFrontNeg1();

// LN 5026-5030 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5036-5040 //<<kn-kn
pos_dir_k = false;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5046-5050 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
end_needle = 'b';
mid_needle = 'f';
knitEndNElseMid();

// LN 5056-5060 //<<kn-kn
pos_dir_k = false;
end_needle = 'b';
mid_needle = 'f';
knitEndNElseMid();

// LN 5066-5070 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
end_needle = 'f';
mid_needle = 'b';
knitEndNElseMid();

// LN 5076-5080 //<<kn-kn
pos_dir_k = false;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5086-5090 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5096-5100 //<<kn-kn
arr_jac_1.splice(3, 0, (arr_jac_1[3] - 1));
pos_dir_k = false;
jacKnit_1();


// LN 5106-5110 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5116-5120 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 3, (arr_jac_1[2] + 1));
remove(arr_jac_1, 6, 1);
insert(arr_jac_1, 8, (arr_jac_1[8] - 1));
remove(arr_jac_1, 17, 3);
insert(arr_jac_1, 19, (arr_jac_1[19] - 1));
remove(arr_jac_1, 21, 1);
insert(arr_jac_1, 23, (arr_jac_1[22] + 1), (arr_jac_1[22] + 2), (arr_jac_1[22] + 3), (arr_jac_1[22] + 4));
remove(arr_jac_1, 33, 3);
jacKnit_1();

// LN 5126-5130 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5136-5140 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 5, 1);
insert(arr_jac_1, 7, (arr_jac_1[7] - 1));
insert(arr_jac_1, 19, (arr_jac_1[19] - 1));
remove(arr_jac_1, 22, 1);
remove(arr_jac_1, 28, 1);
jacKnit_1();

// LN 5146-5150 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5156-5160 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 4, 1);
insert(arr_jac_1, 6, (arr_jac_1[6] - 1));
insert(arr_jac_1, 17, (arr_jac_1[16] + 1), (arr_jac_1[16] + 2), (arr_jac_1[16] + 3));
remove(arr_jac_1, 27, 2);
insert(arr_jac_1, 33, (arr_jac_1[32] + 1), (arr_jac_1[32] + 2), (arr_jac_1[32] + 3));
jacKnit_1();

// LN 5166-5170 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5176-5180 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 4, (arr_jac_1[3] + 1));
insert(arr_jac_1, 7, (arr_jac_1[7] - 1));
remove(arr_jac_1, 10, 1);
remove(arr_jac_1, 18, 3);
remove(arr_jac_1, 20, 2);
insert(arr_jac_1, 21, (arr_jac_1[20] + 1));
remove(arr_jac_1, 30, 3);
jacKnit_1();

// LN 5186-5190 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5196-5200 //<<kn-kn
pos_dir_k = false;
insert(arr_jac_1, 5, (arr_jac_1[4] + 1));
remove(arr_jac_1, 10, 1);
remove(arr_jac_1, 22, 1);
insert(arr_jac_1, 23, (arr_jac_1[22] + 1), (arr_jac_1[22] + 2));
remove(arr_jac_1, 26, 1);
jacKnit_1();

// LN 5206-5210 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5216-5220 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 3, 1);
insert(arr_jac_1, 5, (arr_jac_1[4] + 1));
remove(arr_jac_1, 9, 1);
insert(arr_jac_1, 13, (arr_jac_1[13] - 2), (arr_jac_1[13] - 1));
insert(arr_jac_1, 17, (arr_jac_1[16] + 1), (arr_jac_1[16] + 2));
insert(arr_jac_1, 21, (arr_jac_1[20] + 1), (arr_jac_1[20] + 2), (arr_jac_1[20] + 3));
insert(arr_jac_1, 26, (arr_jac_1[26] - 1));
insert(arr_jac_1, 33, (arr_jac_1[33] - 2), (arr_jac_1[33] - 1));
insert(arr_jac_1, 37, (arr_jac_1[36] + 1), (arr_jac_1[36] + 2));
insert(arr_jac_1, 41, (arr_jac_1[40] + 1), (arr_jac_1[40] + 2), (arr_jac_1[40] + 3));
jacKnit_1();

// LN 5226-5230 //>>kn-kn
carrier = carrier_5;
pos_dir_k = true;
bed_1 = 'f';
bed_2 = 'b';
jacKnit_1();

// LN 5236-5240 //<<kn-kn
pos_dir_k = false;
remove(arr_jac_1, 3, 1);
remove(arr_jac_1, 7, 1);
insert(arr_jac_1, 24, (arr_jac_1[24] - 1));
remove(arr_jac_1, 27, 1);
remove(arr_jac_1, 27, 1);
remove(arr_jac_1, 29, 1);
jacKnit_1();

// LN 5246-5250 //>>kn-kn
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_1();

// LN 5256-5370 //<<kn-kn //>>kn-kn
for (rep = 0; rep < 3; ++rep) {
    if (rep > 0) {
        carrier = carrier_5;
        pos_dir_k = true;
        end_needle = 'f';
        mid_needle = 'b';
        knitEndNElseMid();
        pos_dir_k = false;
        knitEndNElseMid();
        carrier = carrier_4;
        pos_dir_k = true;
        end_needle = 'b';
        mid_needle = 'f';
        knitEndNElseMid();
    }
    if (rep < 2) {
        carrier = carrier_4;
        pos_dir_k = false;
        end_needle = 'b';
        mid_needle = 'f';
        knitEndNElseMid();
    }
}

// LN 5378-5472
array = arr_jac2_1;
jacPattern_2();

// LN 5478-5631
array = arr_jac2;
jacPattern_2();

// LN 5638-5732
array = arr_jac2_2;
jacPattern_2();
carrier = carrier_4;
pos_dir_k = true;
bed_1 = 'b';
bed_2 = 'f';
jacKnit_2();

// left off at LN 5738

// // 5808-5862
// patDivider();

// begin single jersey
// 5868


console.log(arr_checker_1);
console.log(arr_checker_2);
console.log(arr_lace_1);
console.log(arr_lace_2);
console.log(arr_lace_3);
console.log(arr_2rib_x_1);
console.log(arr_2rib_x_2);
console.log(arr_2rib_k_1);
console.log(arr_2rib_k_2);
console.log(arr_jac_1);
// console.log(result);
console.log(arr_jac2_1);
console.log(arr_jac2_2);
console.log(arr_jac2);

k.write('kniterate.k');