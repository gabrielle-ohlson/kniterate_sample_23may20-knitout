// import the knitoutWriter code and instantiate it as an object
const knitout = require('knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6'] });

// add some headers relevant to this job
k.addHeader('Machine', 'Kniterate');
k.addHeader('Gauge', '7');

console.warn('DIFF: kniterate carriers start on left side');
console.warn('how to decide what needle index is? count from 1 or with negatives?');
// swatch variables
const carrier_1 = '1'; // carrier for draw thread
const carrier_2 = '2'; // carrier for waste yarn
const carrier_3 = '3'; // carrier for right neck-armhole
const carrier_4 = '4'; // carrier for left neck-armhole/main/jacquard background
const carrier_5 = '5'; // carrier for jacquard foreground
const carrier_6 = '6'; // carrier for color divider
let initial_width = 96;
// 252 needles in kniterate bed
const left_needle = 77;
const right_needle = 172;
const center_needle = Math.round(((left_needle - (right_needle - 1))/2) + right_needle);
k.rack(0);
console.warn('DIFF: need to !home! and rack first');

// LN 21-135
// knit on alternate needles f/b bed - not sure why (?) since do waste yarn after anyway
// do for all carriers that will be used
for (let c = '1'; c <= 6; ++c) {
// convert c to a string so it can be used as a carrier name
    c = c.toString();
    k.inhook(c);
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, c);
        } else {
            k.knit('+', 'b' + s, c);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (s % 2 === 0) {
            k.knit('-', 'f' + s, c);
        } else {
            k.knit('-', 'b' + s, c);
        }
    }
    k.releasehook(c);
}
console.warn("DIFF: kniterate doesn't use hooks");

//***REUSABLE FUNCTIONS***:
////////////////
// base funcs //
////////////////
// func for knitting on alternate needles switching btw front & back bed, starting in pos dir
function knitAltNPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (s % 2 !== 0) {
            k.knit('+', 'f' + s, carrier);
        } else {
            k.knit('+', 'b' + s, carrier);
        }
    }
    for (let s = right_needle; s >= left_needle; --s) {
        if (alt_pos) {
            if (s % 2 === 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        } else {
            if (s % 2 !== 0) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }
        }
    }
}
// func for knit on alt needles switching btw front & back bed, starting in pos dir
function knitAltBedsPos() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'b' + s,  carrier);
    }
}
console.warn('do I need the miss^? Q applies to other functions, i.e. knitAltPos & xferAltN');
// func for knit switch beds start neg
function knitAltBedsNeg() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.knit('-', 'f' + s, carrier);
    }
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'b' + s, carrier);
        // k.miss('+', 'b' + s, carrier);
    }
}
// func for dropping on front bed in pos dir
function dropFront() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.drop('f' + s);
    }
}
// func for missing on back bed in neg dir, once
function missAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        // k.miss('-', 'f' + s, carrier);
        k.miss('-', 'b' + s, carrier);
    }
}
console.warn('missAllPos1 & missAllNeg1 create floats when including the front bed (but k-code includes miss on both beds, as kn-kn), but maybe bc kniterate xfer op is directional? For now, k.miss for front is commented out');
// func for knit all needles on both beds in pos dir, once
function knitAllPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
        k.knit('+', 'b' + s, carrier);
    }
}
// func for knit all needles on both beds in neg dir, once
function knitAllNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
        k.knit('-', 'b' + s, carrier);
    }
}
// func for xfer every other N from one bed to other bed
function xferAltN() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (s % 2 !== 0) {
                k.xfer(give_needle + s, receive_needle + s);
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (alt_pos) {
                if (s % 2 === 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            } else {
                if (s % 2 !== 0) {
                    k.xfer(give_needle + s, receive_needle + s);
                }
            }
        }
    }
}
// func for knitting all N on front bed in pos dir, once
function knitFrontPos1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        k.knit('+', 'f' + s, carrier);
    }
}
// func for knitting all N on front bed in neg dir, once
function knitFrontNeg1() {
    for (let s = right_needle; s >= left_needle; --s) {
        k.knit('-', 'f' + s, carrier);
    }
}
// func for knitting divider rows btw diff patterns
function patDivider() {
    carrier = carrier_6;
    for (let r = 0; r < 6; r += 2) {
        knitFrontPos1();
        knitFrontNeg1();
    }
}
//////////////////
// 2x2Rib funcs //
//////////////////
// generate base array for 1st xfer sequence in 2x2Rib pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_x_1 = [];
(function arr2RibXRange_1() {
    let i = 1;
    while (i <= 252) {
        i += 2;
        if (i >= left_needle && i <= right_needle) {
            arr_2rib_x_1.push(i);
        }
        i += 5;
        if (i >= left_needle && i <= right_needle) {
            arr_2rib_x_1.push(i);
        }
    }
})();
// generate base array for 2nd xfer sequence in 2x2Rib pat, including whole needle bed (then altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_x_2 = [];
(function arr2RibXRange_2() {
  for (let i = -5; i <= 252; i += 7) {
    if (i >= left_needle && i <= right_needle) {
        arr_2rib_x_2.push(i);
    }
  }
})();
// generate base array for 1st knit sequence in 2x2Rib pat, including whole needle bed (then in func, altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_k_1 = [];
(function arr2RibKRange_1() {
    for (let i = -3; i <= 252; i += 4) {
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        ++i;
        arr_2rib_k_1.push(i);
        if (i >= 252) {
            break;
        }
    }
})();
// generate base array for 2st knit sequence in 2x2Rib pat, including whole needle bed (then in func, altered according to left/right-most needle variables) so can be reused (IIFE)
let arr_2rib_k_2 = [];
(function arr2RibKRange_2() {
    for (let i = -3; i <= 252; i += 6) {
        arr_2rib_k_2.push(i);
        ++i;
        arr_2rib_k_2.push(i);
        if (i >= 252) {
            break;
        }
    }
})();
// func for pos dir xfer in 2x2Rib pat
function rib2x2Xfer_1() {
    for (let s = left_needle; s <= right_needle; ++s) {
        if (arr_2rib_x_1.includes(s) === true) {
            k.xfer(give_needle + s, receive_needle + s);
        }
    }
}
// func for neg dir xfer in 2x2Rib pat
function rib2x2Xfer_2() {
    if (neg_dir_x) {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_x_2.includes(s) === true && (s + gs) < right_needle) {
                k.xfer(give_needle + (s + gs), receive_needle + (s + rs));
            }
        }
    } else {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_x_2.includes(s) === true && (s + gs) <= right_needle) {
                k.xfer(give_needle + (s + gs), receive_needle + (s + rs));
            }            
        }
    }
}
// 1st func for knitting 2x2Rib, can toggle btw pos & neg dir
function rib2x2Knit_1() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_k_1.includes(s) === true) {
                k.knit('+', 'f' + s, carrier);
            } else {
                k.knit('+', 'b' + s, carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_k_1.includes(s) === true) {
                k.knit('-', 'f' + s, carrier);
            } else {
                k.knit('-', 'b' + s, carrier);
            }            
        }
    }
}
// 2nd func for knitting 2x2Rib, can toggle btw pos & neg dir
function rib2x2Knit_2() {
    if (pos_dir_k) {
        for (let s = left_needle; s <= right_needle; ++s) {
            if (arr_2rib_k_2.includes(s) === true) {
                k.knit('+', 'f' + (s + fs), carrier);
            }
            if (arr_2rib_k_1.includes(s) !== true) {
                k.knit('+', 'b' + (s + bs), carrier);
            }
        }
    } else {
        for (let s = right_needle; s >= left_needle; --s) {
            if (arr_2rib_k_2.includes(s) === true && (s + fs) <= right_needle) {
                k.knit('-', 'f' + (s + fs), carrier);
            }
            if (arr_2rib_k_1.includes(s) !== true && (s + bs) <= right_needle) {
                k.knit('-', 'b' + (s + bs), carrier);
            }
        }
    }
}
// func for 2x2rib pattern repeat
function rib2x2Pattern() {
// LN 3048-3091 (first time around)
    carrier = carrier_4;
    for (let r = 0; r < 6; r += 2) {
        pos_dir_k = true;
        rib2x2Knit_1();
        pos_dir_k = false;
        rib2x2Knit_1();
    }
    if (rib_rep !== (last_rib_rep - 1)) {
    // 3096-3100
        pos_dir_k = true;
        fs = 0;
        bs = 0;
        rib2x2Knit_2();
    // LN 3104-3108
        give_needle = 'f';
        receive_needle = 'b';
        gs = 2; // could alternatively be -5
        rs = 2;
        rib2x2Xfer_2();
    // LN 3112-3116
        neg_dir_x = false;
        gs = 3;
        rs = 3;
        rib2x2Xfer_2();
    // LN 3120-3124 //knit in neg dir once
        pos_dir_k = false;
        fs = 2;
        bs = 0;
        rib2x2Knit_2();
    // LN 3128-2132 //F xfer pos dir
        gs = 4;
        rs = 4;
        rib2x2Xfer_2();
    // LN 3136-3140 //F xfer neg dir
        neg_dir_x = true;
        gs = 5;
        rs = 5;
        rib2x2Xfer_2();
    // LN 3143-3150 //B xfer pos dir
        neg_dir_x = false;
        k.rack(-2);
        give_needle = 'b';
        receive_needle = 'f';
        gs = 4; // could alternatively be -3
        rs = 2; // could alternatively be -5
        rib2x2Xfer_2();
    // LN 3154-3158 //B xfer neg dir
        neg_dir_x = true;
        gs = 5;
        rs = 3;
        rib2x2Xfer_2();
    // LN 3161-3169 //B xfer pos dir
        neg_dir_x = false;
        k.rack(2);
        gs = 2;
        rs = 4;
        rib2x2Xfer_2();
    // LN 3173-3180 //B xfer neg dir
        neg_dir_x = true;
        gs = 3;
        rs = 5;
        rib2x2Xfer_2();
        k.rack(0);
    } else {
    // LN 3778-3782 //B xfer pos dir
        give_needle = 'b';
        receive_needle = 'f';
        rib2x2Xfer_1();
    // LN 3788-3792 //B xfer neg dir
        neg_dir_x = true;
        give_needle = 'b';
        receive_needle = 'f';
        gs = 0;
        rs = 0;
        rib2x2Xfer_2();
    }
}

//***CALLING FUNCTIONS***:
// LN 141-905
// knit waste yarn (start in pos dir)
alt_pos = true;
for (let r = 0; r < 78; r += 2) {
    carrier = carrier_2;
    knitAltNPos();
}

// LN 911-945
// knit some rows switching between front & back bed
for (let r = 0; r < 4; r += 2) {
    carrier = carrier_2;
    knitAltBedsPos();
}

// LN 951-965
// drop front
dropFront();
console.warn('seems to knit without carrier to "drop front"... how would I do this? no drop operation in kniterate');
carrier = carrier_2;
missAllNeg1();

// LN 971-985
// draw thread
for (let s = left_needle; s <= right_needle; ++s) {
    k.knit('+', 'b' + s, carrier_1);
}
for (let s = right_needle; s >= left_needle; --s) {
    k.miss('-', 'b' + s, carrier_1);
}

// LN 988-999
// not 100% sure what's happening here tbh
k.rack(0.5);
carrier = carrier_4;
knitAllPos1();
k.rack(0);
console.warn('DIFF: knitout gets mad when rack < 1');
console.warn('looks weird when knit on both beds at same time... why? is this just the way it visually represents it?');

// LN 1004-1158
for (let r = 0; r < 16; r += 2) {
    carrier = carrier_4;
    knitAltBedsNeg();
}

// LN 1164-1168
carrier = carrier_4;
console.warn("^Do I need to specify this again? Since carrier hasn't changed.");
knitAllNeg1();

// LN 1174-1188
neg_dir_x = false;
give_needle = 'b';
receive_needle = 'f';
xferAltN();
neg_dir_x = true;
xferAltN();
console.log("^NOTE TO SELF: looks like left_needle shifts 1 to left in k-code, but that's just because there is no # signaling carrier on left side bc no carrier used for xfer op");

// LN 2963-3017
patDivider();

// begin 2x2 cable pattern

// LN 3026-3030
give_needle = 'f';
receive_needle = 'b';
rib2x2Xfer_1();

// LN 3036-2040
neg_dir_x = true;
give_needle = 'f';
receive_needle = 'b';
gs = 0;
rs = 0;
rib2x2Xfer_2();

// start repeat
// LN 3048-3792
let last_rib_rep = 6;
for (rib_rep = 0; rib_rep < last_rib_rep; ++rib_rep) {
    rib2x2Pattern();
}
// LN 3048-3180 = 1st repeat
// LN 3185-3317 = 2nd repeat
// LN 3321-3453 = 3rd repeat
// LN 3457-3589 = 4th repeat
// LN 3593-3724 = 5th repeat
// LN 3728-3792 = 6th & final repeat (partial repeat)

// LN 3798-3852
patDivider();

k.write('kniterate.k');