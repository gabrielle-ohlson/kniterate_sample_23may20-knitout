// import the knitoutWriter code and instantiate it as an object
const knitout = require('../../knitout-frontend-js/knitout');
let k = new knitout.Writer({ carriers: ['1', '2', '3', '4', '5', '6', '7', '8'] });

// add some headers relevant to this job
k.addHeader('Machine', 'SWGXYZ');
k.addHeader('Gauge', '15');

// swatch variables
const height = 20;
let width = 10;
const carrier = '6';
// bring in carrier using yarn inserting hook
k.inhook(carrier);

// tuck on alternate needles to cast on
for (let s = width; s > 0; s--) {
  if (s % 2 === 0) {
    k.tuck('-', 'f' + s, carrier);
  } else {
    k.miss('-', 'f' + s, carrier);
  }
}
for (let s = 2; s <= width; s++) {
  if (s % 2 !== 0) {
    k.tuck('+', 'f' + s, carrier);
  } else {
    k.miss('+', 'f' + s, carrier);
  }
}

// release the yarn inserting hook
k.releasehook(carrier);

// knit two rows
for (let s = width; s > 0; s--) {
    k.knit('-', 'f' + s, carrier);
}
for (let s = 1; s <= width; s++) {
    k.knit('+', 'f' + s, carrier);
}

// define variables with let (altered as swatch is knit)
let current_height = 2;
let left_needle = 1;
let right_needle = 10;
let inc_needleR = 3; // needle that will be left empty during Right inc op
let inc_needleL = 7; // needle that will be left empty during Left inc op

// while loop to execute knit/xfer functions until swatch is desired height
while (current_height < height - 2) {
    incRight();
    backwardsRight();
    incLeft();
    backwardsLeft();
}

// assumes swatch already on needles

// transfer stitches from front bed (starting on inc_needleR) to the back bed (+)
    // pos direction because not knitting (so same dir as prev row)
// rack +1
// transfer stitches back to the front bed (inc 1 on right because of rack) (+)
    // transfer to front needle 's + 1' because +1 rack adds 1 to index of parallel needle
// rack 0 (back to starting position [needles across from each other])
// increment index# of right-most needle (the one added to swatch from inc)
function incRight() {
    for (let s = inc_needleR; s <= right_needle; s++) {
      k.xfer('f' + s, 'b' + s);
    }
    k.rack(1);
    for (let s = inc_needleR; s <= right_needle; s++) {
      k.xfer('b' + s, 'f' + (s + 1));
    }
    k.rack(0);
    right_needle++;
}
// knit pass with a backwards loop just on the empty inc_needleR (-)
    // neg direction because last knit pass was pos
    // start at right-most needle (incr. in the incRight func), condition for exec. = >= left_needle, decrementing as run thru code block
// increment current height because knit a row (3 rows total after 1st time code executes)
function backwardsRight() {
    for (let i = right_needle; i >= left_needle; i--) {
        if (i == inc_needleR) {
            k.knit('+', 'f' + i, carrier);
        } else {
            k.knit('-', 'f' + i, carrier);
        }
    }
    current_height++;
}

// transfer stitches from front bed (starting on inc_needleL) to the back bed (-)
    // neg direction because not knitting (so same dir as prev row)
// rack -1
// transfer stitches back to the front bed (inc 1 on left because of rack) (-)
    // transfer to front needle 's - 1' because -1 rack subtracts 1 to index of parallel receiving needle
// rack 0 (back to starting position [needles across from each other])
// decrement index# of left-most needle (the one added to swatch from inc)
function incLeft() {
    for (let s = inc_needleL; s >= left_needle; s--) {
      k.xfer('f' + s, 'b' + s);
    }
    k.rack(-1);
    for (let s = inc_needleL; s >= left_needle; s--) {
      k.xfer('b' + s, 'f' + (s - 1));
    }
    k.rack(0);
    left_needle--;
}
// knit pass with a backwards loop just on the empty inc_needleL (+)
    // pos direction because last knit pass was neg
    // start at left-most needle (incr. in the incLeft func), condition for exec. = <= right_needle, incrementing as run thru code block
// increment current height because knit a row (4 rows total after 1st time code executes)
function backwardsLeft() {
    for (let i = left_needle; i <= right_needle; i++) {
      if (i == inc_needleL) {
        k.knit('-', 'f' + i, carrier);
      } else {
        k.knit('+', 'f' + i, carrier);
      }
    }
    current_height++;
}

// knit two rows
for (let s = right_needle; s >= left_needle; s--) {
    k.knit('-', 'f' + s, carrier);
}
for (let s = left_needle; s <= right_needle; s++) {
    k.knit('+', 'f' + s, carrier);
}

// bring the yarn out with the yarn inserting hook
k.outhook(carrier);

// write the knitout to a file called "inc_rect.k"
k.write('inc_rect.k');
